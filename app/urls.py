from django.urls import path
from .views import home, produtos, nosotros, contacto 

urlpatterns = [
    path('', home, name="home"),
    path('contacto/', contacto, name="contacto"),
    path('nosotros/', nosotros, name="nosotros"),
    path('productos', produtos, name="productos"),
]