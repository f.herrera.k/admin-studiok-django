from django.shortcuts import render
from .models import Producto

# Create your views here.

def home(request):    
    return render(request, 'app/home.html')

def produtos(request):
    productos = Producto.objects.all()
    data = {
        'productos': productos
    }
    return render(request, 'app/productos.html', data)    

def nosotros(request):
    return render(request, 'app/nosotros.html') 

def contacto(request):
    return render(request, 'app/contacto.html') 